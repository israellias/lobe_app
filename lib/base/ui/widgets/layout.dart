import 'dart:io';

import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:lobe/base/colors.dart';
import 'package:lobe/base/ui/widgets/logo_header.dart';
import 'package:lobe/base/utils.dart';

class Layout extends StatelessWidget {
  final Widget child;
  final Widget header;
  final Widget options;

  const Layout({
    Key key,
    @required this.child,
    this.header,
    this.options,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
      FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    }
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.light,
      ),
      child: Scaffold(
        appBar: _CustomAppBar(
          header: header,
          options: options,
        ),
        body: child,
      ),
    );
  }
}

class _CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget header;
  final Widget options;

  const _CustomAppBar({
    Key key,
    this.header,
    this.options,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Stack(
        children: <Widget>[
          SafeArea(
            child: Container(
              padding: EdgeInsets.only(left: 16, top: 8),
              child: options ?? CircularProfileAvatar(
                gravatar('israelliasbo@gmail.com'),
                radius: 24,
                borderWidth: 2,
                borderColor: AppColor.springGreen,
              ),
            ),
          ),
          SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.topCenter,
                  child: header ?? LogoHeader(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(80);
}
