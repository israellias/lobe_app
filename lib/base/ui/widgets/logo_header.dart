import 'package:flutter/material.dart';

class LogoHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxHeight: 48),
      margin: EdgeInsets.symmetric(vertical: 4),
      child: Image(
        image: AssetImage('assets/img/logo.png'),
      ),
    );
  }
}
