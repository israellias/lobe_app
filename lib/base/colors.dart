import 'package:flutter/material.dart';

class AppColor {
  static const int _primaryValue = 0xFF3b00ff;
  static const blue = Color(_primaryValue);
  static const bunker = Color(0xFF151617);
  static const mustard = Color(0xFFfbe354);
  static const osloGray = Color(0xFF81889a);
  static const springGreen = Color(0xFF00ffb2);
  static const steelGray = Color(0xFF1f2431);
  static const background = Color(0xFFFBFBFB);
  static const ringColor = Color.fromRGBO(59, 130, 246, 0.5);
  static const white = Color(0xFFffffff);

  static const MaterialColor primaryColor = MaterialColor(
    _primaryValue,
    <int, Color>{
      50: Color(0xFFCEC2FF),
      100: Color(0xFFB39CFF),
      200: Color(0xFF9270FF),
      300: Color(0xFF7045FF),
      400: Color(0xFF5521FF),
      500: Color(_primaryValue),
      600: Color(0xFF3D00F2),
      700: Color(0xFF3F00DE),
      800: Color(0xFF4100CC),
      900: Color(0xFF4200AD),
    },
  );
}
