import 'dart:convert';

import 'package:crypto/crypto.dart';

String gravatar(String email) {
  String gravatarHash =
      email != null ? md5.convert(utf8.encode(email)).toString() : null;
  return 'https://www.gravatar.com/avatar/$gravatarHash?d=identicon&s=75';
}
