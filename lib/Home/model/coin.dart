import 'package:equatable/equatable.dart';

class Coin extends Equatable {
  final String id;
  final String name;
  final String image;
  final double currentPrice;
  final String symbol;
  final double delta24h;
  final double delta24hPercentage;

  Coin({
    this.symbol,
    this.delta24h,
    this.id,
    this.name,
    this.image,
    this.currentPrice,
    this.delta24hPercentage,
  });

  @override
  List<Object> get props => [
        this.symbol,
        this.delta24h,
        this.id,
        this.name,
        this.image,
        this.currentPrice,
        this.delta24hPercentage,
      ];

  Coin.fromJson(dynamic data)
      : symbol = data['symbol'],
        delta24hPercentage = data['price_change_percentage_24h'],
        id = data['id'],
        name = data['name'],
        image = data['image'],
        currentPrice = double.tryParse(data['current_price'].toString()),
        delta24h = data['price_change_24h'];
}
