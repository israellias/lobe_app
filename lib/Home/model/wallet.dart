import 'package:equatable/equatable.dart';
import 'package:lobe/Balance/model/transaction.dart';
import 'package:lobe/Home/model/coin.dart';

class Wallet extends Equatable {
  final Coin coin;
  final double balance;
  final List<Transaction> transactions;
  final DateTime updatedAt;

  Wallet({
    this.coin,
    this.balance,
    this.transactions,
    this.updatedAt,
  });

  double get usdBalance => this.balance * coin.currentPrice;

  @override
  List<Object> get props => [
        this.coin,
        this.balance,
        this.transactions,
        this.updatedAt,
      ];

  Wallet.fromJson(Map<String, dynamic> data)
      : balance = data['balance'],
        coin = Coin.fromJson(data['coin']),
        transactions = Transaction.fromListJson(data['transactions']),
        updatedAt = data['updated_at'];
}
