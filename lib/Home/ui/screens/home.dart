import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/material.dart';
import 'package:lobe/Home/ui/widgets/wallet_carousel.dart';
import 'package:lobe/base/colors.dart';
import 'package:lobe/base/ui/widgets/layout.dart';
import 'package:lobe/base/utils.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Layout(
      child: Center(
        child: Padding(
          padding: EdgeInsets.only(bottom: 24, left: 16, right: 16),
          child: Column(
            children: [
              Container(
                alignment: Alignment.topLeft,
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  'Tu plata, así de simple',
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 22,
                  ),
                ),
              ),
              Expanded(
                child: WalletCarousel(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
