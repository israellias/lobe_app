import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Home/provider/current_wallet_provider.dart';
import 'package:lobe/Home/provider/wallets_provider.dart';
import 'package:lobe/Swap/provider/swap_provider.dart';
import 'package:lobe/Swap/provider/to_wallets_provider.dart';
import 'package:lobe/base/colors.dart';

class WalletActions extends ConsumerWidget {
  final Wallet wallet;

  WalletActions({this.wallet});

  @override
  Widget build(BuildContext context, watch) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Align(
            alignment: Alignment.centerLeft,
            child: TextButton.icon(
              icon: Icon(
                Icons.arrow_forward_ios,
                color: AppColor.white,
              ),
              label: Flexible(
                child: Text(
                  'Ver transacciones',
                  style: TextStyle(
                    color: AppColor.white,
                  ),
                ),
              ),
              onPressed: () {
                context.read(currentWalletProvider).state = wallet;
                Navigator.pushNamed(context, '/balance/');
              },
            ),
          ),
        ),
        ElevatedButton.icon(
          icon: Icon(Icons.compare_arrows),
          onPressed: () {
            List<Wallet> wallets = context.read(walletsProvider);

            // Filling destination wallets
            final toWalletsNotifier = context.read(toWalletsProvider.notifier);
            final toWallets = List<Wallet>.from(wallets.where(
              (w) => w.coin.id != wallet.coin.id,
            ));
            toWalletsNotifier.state = toWallets;

            // Configuring initial Swap
            final notifier = context.read(swapProvider.notifier);
            notifier.initSwap(wallet, toWallets[0]);

            // Configure next carousel
            context.read(toWalletCarouselProvider).state = 0;
            // Go to swap page
            Navigator.pushNamed(context, '/swap/');
          },
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
            primary: AppColor.springGreen,
            onPrimary: Colors.black,
          ),
          label: Text(
            'Transferir',
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
        ),
      ],
    );
  }
}
