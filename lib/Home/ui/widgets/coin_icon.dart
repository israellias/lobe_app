import 'package:flutter/material.dart';

class CoinIcon extends StatelessWidget {
  final String symbol;
  final int size;

  const CoinIcon({Key key, this.symbol, this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image(
      image: NetworkImage(
        'https://cryptoicons.org/api/white/$symbol/$size',
      ),
      errorBuilder: (context, _, __) => Image(
        image: AssetImage('assets/icon/$symbol.png'),
        width: size.toDouble(),
        height: size.toDouble(),
        errorBuilder: (context, _, __) => SizedBox(
          width: size.toDouble(),
          height: size.toDouble(),
        ),
      ),
      width: size.toDouble(),
      height: size.toDouble(),
      fit: BoxFit.cover,
    );
  }
}
