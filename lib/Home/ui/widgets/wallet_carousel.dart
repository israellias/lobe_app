import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Home/provider/wallets_provider.dart';
import 'package:lobe/Home/ui/widgets/wallet_card.dart';

class WalletCarousel extends StatefulWidget {
  @override
  _WalletCarouselState createState() => _WalletCarouselState();
}

class _WalletCarouselState extends State<WalletCarousel> {
  CarouselController carouselController = CarouselController();

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Consumer(
      builder: (context, watch, child) {
        // Wallets' info
        final notifier = watch(walletsProvider.notifier);
        List<Wallet> wallets = watch(walletsProvider);
        if (wallets.length == 0) notifier.loadData();

        return CarouselSlider.builder(
          itemBuilder: (BuildContext context, index, realIndex) => WalletCard(
            index: index,
            wallet: wallets[index],
            height: 250,
            onTap: () {
              carouselController.animateToPage(index);
            },
          ),
          itemCount: wallets.length,
          carouselController: carouselController,
          options: CarouselOptions(
            initialPage: 0,
            enableInfiniteScroll: false,
            enlargeCenterPage: true,
            scrollDirection: Axis.vertical,
            enlargeStrategy: CenterPageEnlargeStrategy.height,
            viewportFraction: height < 700 ? 0.5 : 0.3,
            onPageChanged: (int index, reason) {
              context.read(carouselProvider).state = index;
            },
          ),
        );
      },
    );
  }
}
