import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Home/provider/wallets_provider.dart';
import 'package:lobe/Home/ui/widgets/coin_icon.dart';
import 'package:lobe/Home/ui/widgets/wallet_actions.dart';
import 'package:lobe/base/colors.dart';

enum WalletMode {
  simple,
  full,
}

class WalletCard extends ConsumerWidget {
  final int index;
  final Wallet wallet;
  final double height;
  final VoidCallback onTap;
  final WalletMode mode;

  const WalletCard(
      {Key key,
      this.index = 0,
      this.wallet,
      this.height,
      this.onTap,
      this.mode = WalletMode.full})
      : super(key: key);

  @override
  Widget build(BuildContext context, watch) {
    final carouselIndex = watch(carouselProvider).state;
    final isCurrent = carouselIndex == index;

    final closeIndex = (index - carouselIndex).abs() > 1;
    return Opacity(
      opacity: isCurrent ? 1 : 0.95,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          constraints: BoxConstraints(maxHeight: height),
          child: Card(
            // color: AppColor.primaryColor,
            margin: EdgeInsets.all(16),
            child: Stack(
              clipBehavior: Clip.hardEdge,
              children: [
                Positioned.fill(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                      gradient: LinearGradient(
                        colors: [
                          AppColor.primaryColor,
                          AppColor.ringColor.withOpacity(1),
                        ],
                        begin: FractionalOffset(0.0, 0.0),
                        end: FractionalOffset(1.75, 0.7),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: -50,
                  top: -30,
                  child: Opacity(
                    opacity: 0.35,
                    child: CoinIcon(
                      symbol: wallet.coin.symbol,
                      size: 200,
                    ),
                  ),
                ),
                if (!closeIndex)
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          isCurrent ? 'Tienes ${wallet.coin.name}' : '',
                          style: TextStyle(
                            color: AppColor.white,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              wallet.balance.toStringAsFixed(4),
                              style: TextStyle(
                                color: AppColor.white,
                                // fontWeight: FontWeight.w500,
                                fontSize: 36,
                              ),
                            ),
                            SizedBox(width: 8),
                            Text(
                              wallet.coin.symbol.toUpperCase(),
                              style: TextStyle(
                                color: AppColor.white,
                                // fontWeight: FontWeight.w500,
                                fontSize: 36,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '${wallet.usdBalance.toStringAsFixed(2)} USD',
                              style: TextStyle(
                                color: AppColor.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(width: 16),
                            Text(
                              delta24h(),
                              style: TextStyle(color: AppColor.white),
                            ),
                            SizedBox(width: 4),
                            Icon(
                              wallet.coin.delta24hPercentage > 0
                                  ? Icons.arrow_drop_up
                                  : Icons.arrow_drop_down,
                              color: AppColor.white,
                            )
                          ],
                        ),
                        Spacer(),
                        if (isCurrent && mode == WalletMode.full)
                          WalletActions(wallet: wallet),
                      ],
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String delta24h() {
    final double delta = wallet.coin.delta24hPercentage;
    final sign = delta > 0 ? '+' : '';
    return "$sign${delta.toStringAsFixed(1)}%";
  }
}
