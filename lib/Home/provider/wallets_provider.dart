import 'dart:math';

import 'package:dio/dio.dart';
import 'package:lobe/Balance/model/transaction.dart';
import 'package:lobe/Home/api/home_client.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Swap/model/swap.dart';
import 'package:riverpod/riverpod.dart';

class WalletsProvider extends StateNotifier<List<Wallet>> {
  final client = HomeClient(Dio());

  WalletsProvider() : super([]);

  void loadData() async {
    final coins = await client.coinsMarkets();
    state = coins
        .map(
          (coin) => Wallet.fromJson({
            'balance': _loadBalance(coin),
            'coin': coin,
            'updated_at': DateTime.now(),
            'transactions': [
              {
                'date': DateTime.now().subtract(Duration(minutes: 15)),
                'amount': 10.2,
              },
              {
                'date': DateTime.now().subtract(Duration(days: 4)),
                'amount': -5.3,
              }
            ]
          }),
        )
        .toList();
  }

  double _loadBalance(coin) {
    final random = Random();
    double result = random.nextDouble() * 10;
    if (coin['symbol'] == 'btc') result = 0.035;
    if (coin['symbol'] == 'eth') result = 0.45;
    if (coin['symbol'] == 'usdt') result = 3.4;
    if (coin['symbol'] == 'usdc') result = 100;
    if (coin['symbol'] == 'aave') result = 1.56;

    return result;
  }

  void makeSwap(Swap swap) {
    state = state.map((wallet) {
      if (wallet == swap.fromWallet)
        return Wallet(
          balance: wallet.balance - swap.fromAmount,
          coin: wallet.coin,
          updatedAt: DateTime.now(),
          transactions: wallet.transactions
            ..insert(
                0,
                Transaction.fromJson({
                  'date': DateTime.now(),
                  'amount': -swap.fromAmount,
                })),
        );
      if (wallet == swap.toWallet)
        return Wallet(
          balance: wallet.balance + swap.toAmount,
          coin: wallet.coin,
          updatedAt: DateTime.now(),
          transactions: wallet.transactions
            ..insert(
                0,
                Transaction.fromJson({
                  'date': DateTime.now(),
                  'amount': swap.toAmount,
                })),
        );
      return wallet;
    }).toList();
  }
}

final walletsProvider = StateNotifierProvider<WalletsProvider, List<Wallet>>(
  (ref) => WalletsProvider(),
);

final carouselProvider = StateProvider<int>((ref) => 0);
