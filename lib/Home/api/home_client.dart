import 'package:dio/dio.dart';

class HomeClient {
  final Dio client;
  static const String _baseUrl = 'https://api.coingecko.com/api/v3';

  HomeClient(this.client);

  Future<dynamic> ping() async {
    final res = await client.get(_baseUrl);
    return res.data;
  }

  Future<List> coinsMarkets() async {
    final res = await client.get("$_baseUrl/coins/markets", queryParameters: {
      'vs_currency': 'usd',
      'ids': 'bitcoin,tether,usd-coin,ethereum,aave',
      'order': 'market_cap_desc',
      'per_page': 100,
      'page': 1,
      'sparkline': false,
    });
    return res.data;
  }
}
