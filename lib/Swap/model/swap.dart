import 'package:equatable/equatable.dart';
import 'package:lobe/Home/model/wallet.dart';

class Swap extends Equatable {
  final Wallet fromWallet;
  final Wallet toWallet;
  final double fromAmount;
  final double toAmount;

  Swap({
    this.fromWallet,
    this.toWallet,
    this.fromAmount,
    this.toAmount,
  });

  @override
  List<Object> get props => [
        fromWallet,
        toWallet,
        fromAmount,
        toAmount,
      ];
}
