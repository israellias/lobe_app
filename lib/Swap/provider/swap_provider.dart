import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Swap/model/swap.dart';
import 'package:riverpod/riverpod.dart';

class SwapProvider extends StateNotifier<Swap> {
  SwapProvider() : super(null);

  void setFromAmount(double fromAmount) {
    double toAmount = 0.0;
    if (state.toWallet != null && fromAmount != null)
      toAmount = _converse(state.fromWallet, state.toWallet, fromAmount);

    state = Swap(
      fromWallet: state.fromWallet,
      toWallet: state.toWallet,
      fromAmount: fromAmount,
      toAmount: toAmount,
    );
  }

  void setToWallet(Wallet toWallet) {
    double toAmount = _converse(state.fromWallet, toWallet, state.fromAmount);
    state = Swap(
      fromWallet: state.fromWallet,
      toWallet: toWallet,
      fromAmount: state.fromAmount,
      toAmount: toAmount,
    );
  }

  double _converse(Wallet fromWallet, Wallet toWallet, double amount) {
    double fromUsdPrice = 1.0 / fromWallet.coin.currentPrice;
    double toUsdPrice = 1.0 / toWallet.coin.currentPrice;
    return amount * toUsdPrice / fromUsdPrice;
  }

  void initSwap(Wallet fromWallet, Wallet toWallet) {
    state = Swap(
      fromWallet: fromWallet,
      toWallet: toWallet,
      fromAmount: 0,
      toAmount: 0,
    );
  }
}

final swapProvider = StateNotifierProvider<SwapProvider, Swap>(
  (ref) => SwapProvider(),
);
