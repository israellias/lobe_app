import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/model/wallet.dart';

final toWalletsProvider = StateProvider<List<Wallet>>((ref) => []);
final toWalletCarouselProvider = StateProvider<int>((ref) => 0);
