import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Swap/ui/widgets/destination_carousel.dart';
import 'package:lobe/Swap/ui/widgets/from_wallet.dart';
import 'package:lobe/base/ui/widgets/layout.dart';

class SwapScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, watch) {
    return Layout(
      options: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.pop(context),
      ),
      child: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.only(bottom: 24, left: 16, right: 16),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    'Tu plata, podés moverla',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 22,
                    ),
                  ),
                ),
                SizedBox(height: 16),
                FromWallet(),
                Divider(thickness: 2),
                DestinationCarousel(),
                SizedBox(height: 64)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
