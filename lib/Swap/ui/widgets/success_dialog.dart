import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lobe/Swap/model/swap.dart';
import 'package:lobe/Swap/provider/swap_provider.dart';
import 'package:lobe/base/colors.dart';

class SuccessDialog extends ConsumerWidget {
  @override
  Widget build(BuildContext context, watch) {
    final Swap swap = watch(swapProvider);

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
      ),
      child: Container(
        margin: EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(
              'assets/img/check.svg',
              height: 80,
            ),
            SizedBox(height: 32),
            Text(
              'Envío satisfactorio',
              style: TextStyle(
                color: AppColor.steelGray,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  swapFromString(swap),
                  style: TextStyle(
                    color: AppColor.osloGray,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Icon(
                  Icons.arrow_forward,
                  color: AppColor.osloGray,
                  size: 18,
                ),
                Text(
                  swapToString(swap),
                  style: TextStyle(
                    color: AppColor.osloGray,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
            SizedBox(height: 16),
            Container(
              alignment: Alignment.centerRight,
              child: TextButton(
                onPressed: () {
                  // Go to
                  Navigator.pushNamed(context, '/');
                },
                child: Text('Ir a inicio'),
              ),
            )
          ],
        ),
      ),
    );
  }

  String swapFromString(Swap swap) =>
      '${swap.fromAmount} ${swap.fromWallet.coin.symbol.toUpperCase()}';

  String swapToString(Swap swap) =>
      '${swap.toAmount.toStringAsFixed(4)} ${swap.toWallet.coin.symbol.toUpperCase()}';
}
