import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Swap/provider/swap_provider.dart';
import 'package:lobe/Swap/provider/to_wallets_provider.dart';
import 'package:lobe/Swap/ui/widgets/to_wallet.dart';

class DestinationCarousel extends StatefulWidget {
  @override
  _DestinationCarouselState createState() => _DestinationCarouselState();
}

class _DestinationCarouselState extends State<DestinationCarousel> {
  CarouselController carouselController = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        final List<Wallet> toWallets = watch(toWalletsProvider).state;

        return GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: CarouselSlider.builder(
            itemBuilder: (context, index, realIndex) => ToWallet(
              index: index,
              wallet: toWallets[index],
              onTap: () {
                carouselController.animateToPage(index);
                FocusScope.of(context).unfocus();
              },
            ),
            itemCount: toWallets.length,
            carouselController: carouselController,
            options: CarouselOptions(
              initialPage: 0,
              height: 275,
              enableInfiniteScroll: false,
              enlargeCenterPage: true,
              scrollDirection: Axis.vertical,
              enlargeStrategy: CenterPageEnlargeStrategy.height,
              // viewportFraction: 0.35,
              onPageChanged: (int index, reason) {
                context.read(toWalletCarouselProvider).state = index;
                final notifier = context.read(swapProvider.notifier);
                notifier.setToWallet(toWallets[index]);

                // Unfocus
                FocusScope.of(context).unfocus();
              },
            ),
          ),
        );
      },
    );
  }
}
