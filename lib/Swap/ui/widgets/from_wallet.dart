import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Home/ui/widgets/coin_icon.dart';
import 'package:lobe/Swap/provider/swap_provider.dart';
import 'package:lobe/base/colors.dart';

class FromWallet extends ConsumerWidget {
  @override
  Widget build(BuildContext context, watch) {
    final Wallet wallet = watch(swapProvider).fromWallet;

    return Container(
      constraints: BoxConstraints(maxHeight: 175),
      child: Card(
        // color: AppColor.primaryColor,
        // margin: EdgeInsets.all(16),
        child: Stack(
          clipBehavior: Clip.antiAlias,
          children: [
            Positioned(
              left: -50,
              top: -30,
              child: Opacity(
                opacity: 0.6,
                child: ColorFiltered(
                  colorFilter: ColorFilter.mode(AppColor.blue, BlendMode.srcIn),
                  child: CoinIcon(
                    symbol: wallet.coin.symbol,
                    size: 200,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 16, right: 24),
                    alignment: Alignment.centerRight,
                    child: Text(
                      'Mover',
                      style: TextStyle(
                        color: AppColor.steelGray,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.numberWithOptions(
                              decimal: true,
                            ),
                            textAlign: TextAlign.end,
                            autofocus: true,
                            style: TextStyle(
                              color: AppColor.steelGray,
                              fontSize: 36,
                            ),
                            decoration: InputDecoration.collapsed(
                              hintText: '0.0000',
                            ),
                            onChanged: (value) {
                              final notifier = context.read(
                                swapProvider.notifier,
                              );
                              notifier.setFromAmount(double.tryParse(value));
                            },
                          ),
                        ),
                        SizedBox(width: 8),
                        Text(
                          wallet.coin.symbol.toUpperCase(),
                          style: TextStyle(
                            color: AppColor.steelGray,
                            fontSize: 36,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'Disponible:',
                        style: TextStyle(
                          color: AppColor.osloGray,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 8),
                      Text(
                        '${wallet.balance.toStringAsFixed(4)} ${wallet.coin.symbol.toUpperCase()}',
                        style: TextStyle(
                          color: AppColor.osloGray,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 16)
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
