import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Home/ui/widgets/coin_icon.dart';
import 'package:lobe/Swap/model/swap.dart';
import 'package:lobe/Swap/provider/swap_provider.dart';
import 'package:lobe/Swap/provider/to_wallets_provider.dart';
import 'package:lobe/Swap/ui/widgets/to_wallet_actions.dart';
import 'package:lobe/base/colors.dart';

class ToWallet extends ConsumerWidget {
  final int index;
  final Wallet wallet;
  final VoidCallback onTap;

  const ToWallet({
    Key key,
    this.index,
    this.wallet,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, watch) {
    final carouselIndex = watch(toWalletCarouselProvider).state;
    final isCurrent = carouselIndex == index;

    final Swap swap = watch(swapProvider);

    return Opacity(
      opacity: isCurrent ? 1 : 0.5,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          constraints: BoxConstraints(maxHeight: 300),
          child: Card(
            // color: AppColor.primaryColor,
            // margin: EdgeInsets.all(16),
            child: Stack(
              clipBehavior: Clip.antiAlias,
              children: [
                Positioned(
                  left: -50,
                  top: -30,
                  child: Opacity(
                    opacity: 0.9,
                    child: ColorFiltered(
                      colorFilter:
                          ColorFilter.mode(AppColor.mustard, BlendMode.srcIn),
                      child: CoinIcon(
                        symbol: wallet.coin.symbol,
                        size: 200,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 16, right: 24),
                        alignment: Alignment.centerRight,
                        child: Text(
                          'Para obtener',
                          style: TextStyle(
                            color: AppColor.steelGray,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8, right: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Expanded(
                              child: Text(
                                swap.toAmount.toStringAsFixed(4),
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  color: AppColor.steelGray,
                                  fontSize: 36,
                                ),
                              ),
                            ),
                            SizedBox(width: 8),
                            Text(
                              wallet.coin.symbol.toUpperCase(),
                              style: TextStyle(
                                color: AppColor.steelGray,
                                fontSize: 36,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 16),
                        alignment: Alignment.centerRight,
                        child: Text(
                          wallet.coin.name,
                          style: TextStyle(
                            color: AppColor.osloGray,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: 8),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: ToWalletActions(index: index),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
