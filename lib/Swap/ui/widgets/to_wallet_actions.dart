import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Home/provider/wallets_provider.dart';
import 'package:lobe/Swap/model/swap.dart';
import 'package:lobe/Swap/provider/swap_provider.dart';
import 'package:lobe/Swap/provider/to_wallets_provider.dart';
import 'package:lobe/Swap/ui/widgets/success_dialog.dart';
import 'package:lobe/base/colors.dart';

class ToWalletActions extends ConsumerWidget {
  final int index;

  ToWalletActions({this.index});

  @override
  Widget build(BuildContext context, watch) {
    final carouselIndex = watch(toWalletCarouselProvider).state;
    final isCurrent = carouselIndex == index;

    // final Swap swap = watch(swapProvider);

    return Row(
      children: [
        if (isCurrent)
          Expanded(
            child: Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                icon: Icon(
                  Icons.info_outline,
                  size: 12,
                  color: AppColor.osloGray,
                ),
                label: Flexible(
                  child: Text(
                    '¿Cómo se calcula?',
                    style: TextStyle(
                      color: AppColor.osloGray,
                      fontSize: 12,
                    ),
                  ),
                ),
                onPressed: () {},
              ),
            ),
          ),
        if (isCurrent)
          ElevatedButton.icon(
            icon: Icon(Icons.compare_arrows),
            onPressed: () {
              // Configure next carousel
              context.read(carouselProvider).state = 0;

              // MakeSwap
              final Swap swap = context.read(swapProvider);
              final notifier = context.read(walletsProvider.notifier);
              notifier.makeSwap(swap);

              showDialog(
                context: context,
                builder: (context) => SuccessDialog(),
              );
            },
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
              primary: AppColor.springGreen,
              onPrimary: Colors.black,
            ),
            label: Text(
              'Mover aquí',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
      ],
    );
  }
}
