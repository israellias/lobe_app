import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Balance/ui/widgets/transaction_card.dart';
import 'package:lobe/Home/model/wallet.dart';
import 'package:lobe/Home/provider/current_wallet_provider.dart';
import 'package:lobe/Home/provider/wallets_provider.dart';
import 'package:lobe/Home/ui/widgets/wallet_card.dart';
import 'package:lobe/base/ui/widgets/layout.dart';

class BalanceScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, watch) {
    final Wallet wallet = watch(currentWalletProvider).state;
    final carouselIndex = watch(carouselProvider).state;

    return Layout(
      options: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.pop(context),
      ),
      child: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.only(bottom: 24, left: 16, right: 16),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    'Tu plata, esto es lo que ocurrió',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 22,
                    ),
                  ),
                ),
                SizedBox(height: 16),
                WalletCard(
                  index: carouselIndex,
                  wallet: wallet,
                  height: 200,
                  mode: WalletMode.simple,
                ), // FromWallet(),
                Divider(thickness: 2),
                Column(
                  children: wallet.transactions
                      .map<TransactionCard>(
                        (e) => TransactionCard(transaction: e),
                      )
                      .toList(),
                ),
                // DestinationCarousel(),
                SizedBox(height: 64)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
