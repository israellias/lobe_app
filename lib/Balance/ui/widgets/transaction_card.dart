import 'package:flutter/material.dart';
import 'package:lobe/Balance/model/transaction.dart';
import 'package:lobe/base/colors.dart';
import 'package:timeago/timeago.dart' as timeago;

class TransactionCard extends StatelessWidget {
  final Transaction transaction;

  const TransactionCard({Key key, this.transaction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isReceived = transaction.event == TransactionEvent.RECEIVE;
    return Card(
      elevation: 1,
      margin: EdgeInsets.symmetric(vertical: 8),
      child: Padding(
        padding: EdgeInsets.all(8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              isReceived ? Icons.add : Icons.arrow_forward,
              color: AppColor.blue,
              size: 32,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    isReceived ? 'Recibido' : 'Enviado',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: AppColor.steelGray,
                    ),
                  ),
                  Text(
                    timeago.format(transaction.date, locale: 'es'),
                    style: TextStyle(
                      color: AppColor.osloGray,
                    ),
                  )
                ],
              ),
            ),
            Spacer(),
            Text(
              getAmount(transaction.amount),
              style: TextStyle(
                color: isReceived ? AppColor.springGreen : AppColor.steelGray,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            SizedBox(width: 8),
          ],
        ),
      ),
    );
  }

  String getAmount(double amount) {
    final sign = amount.sign >= 0 ? '+' : '';
    return '$sign${amount.toStringAsFixed(4)}';
  }
}
