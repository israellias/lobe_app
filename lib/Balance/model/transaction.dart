import 'package:equatable/equatable.dart';

enum TransactionEvent {
  SEND,
  RECEIVE,
}

class Transaction extends Equatable {
  final TransactionEvent event;
  final double amount;
  final DateTime date;

  Transaction({
    this.event,
    this.amount,
    this.date,
  });

  @override
  List<Object> get props => [
        event,
        amount,
        date,
      ];

  Transaction.fromJson(dynamic data)
      : event = data['amount'] > 0
            ? TransactionEvent.RECEIVE
            : TransactionEvent.SEND,
        amount = data['amount'],
        date = data['date'];

  static List<Transaction> fromListJson(List<Object> data) {
    return data
            ?.map<Transaction>((json) => Transaction.fromJson(json))
            ?.toList() ??
        [];
  }
}
