import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lobe/Balance/ui/screens/balance_screen.dart';
import 'package:lobe/Swap/ui/screens/swap_screen.dart';
import 'package:lobe/base/colors.dart';

import 'Home/ui/screens/home.dart';

void main() {
  runApp(ProviderScope(
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'lobe',
      navigatorKey: _navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: AppColor.primaryColor,
        accentColor: AppColor.springGreen,
        scaffoldBackgroundColor: AppColor.background,
        fontFamily: 'Poppins',
        backgroundColor: AppColor.blue,
        canvasColor: AppColor.white,
        buttonTheme: ButtonThemeData(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
          ),
        ),
        cardTheme: CardTheme(
          shadowColor: AppColor.osloGray,
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(0)),
          ),
        ),
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => Home(),
        '/swap/': (context) => SwapScreen(),
        '/balance/': (context) => BalanceScreen(),
      },
    );
  }
}
